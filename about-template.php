<?php
/**
 * Template Name: About project
 */
?>


<?php while (have_posts()) : the_post(); ?>
	<?php the_content(); ?>
	<div class="swiper-container">
		<div class="swiper-wrapper">

			<div class="swiper-slide">

				<div class="section-content about-section">
					<div class="infoboxes-container">

						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>


					</div>
					<div class="section-copy">
						<h1 class="heading">O PROJEKCIE</h1>
						<p class="copy">
							Wyobraź sobie, że biurowiec Imagine to nie tylko Twoje nowe miejsce pracy, ale po prostu nowe środowisko pracy – miejsce dedykowane rozwijającym się biznesom, innowacyjnym firmom z sektora IT, energetyki i nowych technologii. Imagine oferuje szereg udogodnień, budynki zaś charakteryzuje bezpretensjonalna, nowoczesna architektura i zaawansowane rozwiązania technologiczne.                </p>
					</div>
				</div>

			</div>


			<div class="swiper-slide">
				<div class="section-content location-section">
					<div class="legend-panel">
						<h1>PLAN ZAGOSPODAROWANIA TERENU</h1>
						<img src="<?= get_template_directory_uri();?>/dist/images/legend.jpg" alt="">
					</div>
					<div class="map-container">

					</div>
				</div>



			</div>
			<div class="swiper-slide">
				<div class="section-content environment-section">
					<div class="infoboxes-container">

						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>


					</div>
					<div class="section-copy">
						<h1 class="heading">O PROJEKCIE</h1>
						<p class="copy">
							Wyobraź sobie, że biurowiec Imagine to nie tylko Twoje nowe miejsce pracy, ale po prostu nowe środowisko pracy – miejsce dedykowane rozwijającym się biznesom, innowacyjnym firmom z sektora IT, energetyki i nowych technologii. Imagine oferuje szereg udogodnień, budynki zaś charakteryzuje bezpretensjonalna, nowoczesna architektura i zaawansowane rozwiązania technologiczne.                </p>
					</div>

				</div>
			</div>
		</div>
		<!-- Add Pagination -->
		<div class="swiper-pagination"></div>
	</div>

<?php endwhile; ?>