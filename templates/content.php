<article <?php post_class(); ?>>
<!--    <h2 class="entry-title"><a href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a></h2>-->

    <a href="<?php the_permalink(); ?>">

    <div class="post-date"><?php get_template_part('templates/entry-meta'); ?></div>

  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
    </a>
</article>
