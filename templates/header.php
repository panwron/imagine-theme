<header class="banner">
    <div class="container-fluid">
        <ul class="lang-switch lg">

            <?php $trans = pll_the_languages(array('raw'=>1));?>

            <li class="lang-item"><a class="<?php if ($trans['pl']['current_lang'])  echo 'current-lang'; ?>" href="<?php echo $trans['pl']['url']; ?>">PL</a></li>
            <li class="lang-item"><a class="<?php if ($trans['en']['current_lang'])  echo 'current-lang'; ?>" href="<?php echo $trans['en']['url']; ?>">ENG</a></li>


        </ul>

        <div class="menu-toggle-wrap">
            <a href="#" id="menu-toggle">

            </a>
        </div>






        <div class="row">
            <div class="col-sm-2 brand-logo">
                    <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                        <img class="logo" src="<?= get_template_directory_uri();?>/dist/images/imagine-logo-full.png" alt="">
                    </a>
            </div>

            <nav class="col-sm-10 nav-primary">
                <ul class="lang-switch xs">

                    <?php $trans = pll_the_languages(array('raw'=>1));?>

                    <li class="lang-item"><a class="<?php if ($trans['pl']['current_lang'])  echo 'current-lang'; ?>" href="<?php echo $trans['pl']['url']; ?>">PL</a></li>
                    <li class="lang-item"><a class="<?php if ($trans['en']['current_lang'])  echo 'current-lang'; ?>" href="<?php echo $trans['en']['url']; ?>">ENG</a></li>


                </ul>

		            <?php
		            if (has_nav_menu('primary_navigation')) :
			            wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
		            endif;
		            ?>
            </nav>




        </div>



    </div>
</header>


