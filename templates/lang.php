array(2)
{ ["en"]=> array(10) {
["id"]=> int(3)
["order"]=> int(0)
["slug"]=> string(2) "en"
["locale"]=> string(5) "en-GB"
["name"]=> string(7) "English"
["url"]=> string(41) "//localhost:3000/imagine.dev/web/en/"
["flag"]=> string(71) "//localhost:3000/imagine.dev/web/app/plugins/polylang/flags/gb.png"
["current_lang"]=> bool(false)
["no_translation"]=> bool(false)
["classes"]=> array(4) {
    [0]=> string(9) "lang-item"
    [1]=> string(11) "lang-item-3"
    [2]=> string(12) "lang-item-en"
    [3]=> string(15) "lang-item-first" }

}

["pl"]=> array(10) { ["id"]=> int(6) ["order"]=> int(0) ["slug"]=> string(2) "pl" ["locale"]=> string(5) "pl-PL" ["name"]=> string(6) "Polski" ["url"]=> string(38) "//localhost:3000/imagine.dev/web/" ["flag"]=> string(71) "//localhost:3000/imagine.dev/web/app/plugins/polylang/flags/pl.png" ["current_lang"]=> bool(true) ["no_translation"]=> bool(false) ["classes"]=> array(4) { [0]=> string(9) "lang-item" [1]=> string(11) "lang-item-6" [2]=> string(12) "lang-item-pl" [3]=> string(12) "current-lang" } } }