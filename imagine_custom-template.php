
<?php
/**
 * Template Name: Imagine custom page
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('templates/content', 'page_custom'); ?>
<?php endwhile; ?>
