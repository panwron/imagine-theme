<?php
/**
 * Template Name: Offices Page
 */
?>


<?php while ( have_posts() ) : the_post(); ?>
<!--	--><?php //the_content(); ?><!-- -->
    <div class="swiper-container">
        <div class="swiper-wrapper">

            <div class="swiper-slide">

                <div class="section-content quater-section">
                    <div class="quater-box _rt"
                         style="background-image: url('<?= get_template_directory_uri(); ?>/dist/images/ofice-top-one.jpg')">
                        <svg class="quater-box-info" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 208.6 208.6">
                            <path fill="#00B188" d="M0 0h208.6v208.6H0z"/>
                            <text transform="translate(10.69 177.912)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">U</tspan>
                                <tspan x="15.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">K</tspan>
                                <tspan x="29.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Ł</tspan>
                                <tspan x="41" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">A</tspan>
                                <tspan x="55.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">D</tspan>
                            </text>
                            <text fill="#FFF" transform="translate(86.48 177.912)" font-family="'Lato-Bold'"
                                  font-size="21">

                            </text>
                            <text transform="translate(10.69 200.907)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">GA</tspan>
                                <tspan x="29.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">BI</tspan>
                                <tspan x="49.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">N</tspan>
                                <tspan x="65.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">E</tspan>
                                <tspan x="77.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">T</tspan>
                                <tspan x="89.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">O</tspan>
                                <tspan x="105.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">W</tspan>
                                <tspan x="128.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Y</tspan>
                            </text>
                            <path fill="#FFF" d="M46.8 84.7h96.9v5.4H46.8z"/>
                            <defs>
                                <path id="a" d="M0 0h208.6v208.6H0z"/>
                            </defs>
                            <clipPath id="b">
                                <use xlink:href="#a" overflow="visible"/>
                            </clipPath>
                            <path fill="#FFF"
                                  d="M113.1 84.7H88.5l-1.2-16.3h26.9l-1.1 16.3zm6.8-14.8c-.8-5.6-4.6-10.3-10.3-10.3H91.9c-5.7 0-9.5 4.7-10.3 10.3l-2.3 15.8h43l-2.4-15.8zM99.4 57.8c1.3.2 1.6.2 2.8 0 2.6-.6 4.9-2.3 5.9-4.1 1.1-2.1 1.7-7.5 1.5-9.9-.4-4.4-4.4-6.6-8.8-6.6s-8.4 2.2-8.8 6.6c-.2 2.3.3 7.8 1.5 9.9.9 1.8 3.3 3.5 5.9 4.1m4.9 39.7c-.9 9.2-1.2 18.3-1.9 27.4-.1 1.6-.3 3.5-.2 5.1 0 .6.2 1.1.6 1.2h10.5c.7-.1.8-3.4-.4-3.9l-1.5-1 4.4-27.2c.9-6-11.3-8.2-11.5-1.6m-7 0c-.2-6.6-12.4-4.4-11.4 1.7l4.4 27.2-1.5 1c-1.1.5-1.1 3.9-.4 3.9h10.5c.4-.1.5-.6.6-1.2.1-1.6-.1-3.5-.2-5.1-.7-9.2-1.1-18.3-2-27.5"
                                  clip-path="url(#b)"/>
                            <path fill="#FFF"
                                  d="M72.7 54.8H52.2V52h20.4v2.8zm0 4.6H52.2v-2.8h20.4v2.8zm0 4.7H52.2v-2.8h20.4v2.8zm0 4.7H52.2V66h20.4v2.8zm0 4.7H52.2v-2.8h20.4v2.8zm0 4.7H52.2v-2.8h20.4v2.8zm0 4.6H52.2V80h20.4v2.8z"/>
                        </svg>
                    </div>
                    <div class="quater-box _lt"
                         style="background-image: url('<?= get_template_directory_uri(); ?>/dist/images/ofice-top-two.jpg')">
                        <svg class="quater-box-info" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 208.6 208.6">
                            <path fill="#00B188" d="M0 0h208.6v208.6H0z"/>
                            <text transform="translate(10.69 177.912)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">U</tspan>
                                <tspan x="15.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">K</tspan>
                                <tspan x="29.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Ł</tspan>
                                <tspan x="41" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">A</tspan>
                                <tspan x="55.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">D</tspan>
                            </text>
                            <text fill="#FFF" transform="translate(86.48 177.912)" font-family="'Lato-Bold'"
                                  font-size="21">

                            </text>
                            <text transform="translate(10.69 200.907)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">GA</tspan>
                                <tspan x="29.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">BI</tspan>
                                <tspan x="49.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">N</tspan>
                                <tspan x="65.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">E</tspan>
                                <tspan x="77.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">T</tspan>
                                <tspan x="89.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">O</tspan>
                                <tspan x="105.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">W</tspan>
                                <tspan x="128.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Y</tspan>
                            </text>
                            <path fill="#FFF" d="M46.8 84.7h96.9v5.4H46.8z"/>
                            <defs>
                                <path id="a" d="M0 0h208.6v208.6H0z"/>
                            </defs>
                            <clipPath id="b">
                                <use xlink:href="#a" overflow="visible"/>
                            </clipPath>
                            <path fill="#FFF"
                                  d="M113.1 84.7H88.5l-1.2-16.3h26.9l-1.1 16.3zm6.8-14.8c-.8-5.6-4.6-10.3-10.3-10.3H91.9c-5.7 0-9.5 4.7-10.3 10.3l-2.3 15.8h43l-2.4-15.8zM99.4 57.8c1.3.2 1.6.2 2.8 0 2.6-.6 4.9-2.3 5.9-4.1 1.1-2.1 1.7-7.5 1.5-9.9-.4-4.4-4.4-6.6-8.8-6.6s-8.4 2.2-8.8 6.6c-.2 2.3.3 7.8 1.5 9.9.9 1.8 3.3 3.5 5.9 4.1m4.9 39.7c-.9 9.2-1.2 18.3-1.9 27.4-.1 1.6-.3 3.5-.2 5.1 0 .6.2 1.1.6 1.2h10.5c.7-.1.8-3.4-.4-3.9l-1.5-1 4.4-27.2c.9-6-11.3-8.2-11.5-1.6m-7 0c-.2-6.6-12.4-4.4-11.4 1.7l4.4 27.2-1.5 1c-1.1.5-1.1 3.9-.4 3.9h10.5c.4-.1.5-.6.6-1.2.1-1.6-.1-3.5-.2-5.1-.7-9.2-1.1-18.3-2-27.5"
                                  clip-path="url(#b)"/>
                            <path fill="#FFF"
                                  d="M72.7 54.8H52.2V52h20.4v2.8zm0 4.6H52.2v-2.8h20.4v2.8zm0 4.7H52.2v-2.8h20.4v2.8zm0 4.7H52.2V66h20.4v2.8zm0 4.7H52.2v-2.8h20.4v2.8zm0 4.7H52.2v-2.8h20.4v2.8zm0 4.6H52.2V80h20.4v2.8z"/>
                        </svg>

                    </div>
                    <div class="quater-box _rb"
                         style="background-image: url('<?= get_template_directory_uri(); ?>/dist/images/ofice-top-three.jpg')">
                        <svg class="quater-box-info" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 208.6 208.6">
                            <path fill="#00B188" d="M0 0h208.6v208.6H0z"/>
                            <text transform="translate(10.69 177.912)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">U</tspan>
                                <tspan x="15.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">K</tspan>
                                <tspan x="29.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Ł</tspan>
                                <tspan x="41" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">A</tspan>
                                <tspan x="55.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">D</tspan>
                            </text>
                            <text fill="#FFF" transform="translate(86.48 177.912)" font-family="'Lato-Bold'"
                                  font-size="21">

                            </text>
                            <text transform="translate(10.69 200.907)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">GA</tspan>
                                <tspan x="29.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">BI</tspan>
                                <tspan x="49.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">N</tspan>
                                <tspan x="65.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">E</tspan>
                                <tspan x="77.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">T</tspan>
                                <tspan x="89.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">O</tspan>
                                <tspan x="105.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">W</tspan>
                                <tspan x="128.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Y</tspan>
                            </text>
                            <path fill="#FFF" d="M46.8 84.7h96.9v5.4H46.8z"/>
                            <defs>
                                <path id="a" d="M0 0h208.6v208.6H0z"/>
                            </defs>
                            <clipPath id="b">
                                <use xlink:href="#a" overflow="visible"/>
                            </clipPath>
                            <path fill="#FFF"
                                  d="M113.1 84.7H88.5l-1.2-16.3h26.9l-1.1 16.3zm6.8-14.8c-.8-5.6-4.6-10.3-10.3-10.3H91.9c-5.7 0-9.5 4.7-10.3 10.3l-2.3 15.8h43l-2.4-15.8zM99.4 57.8c1.3.2 1.6.2 2.8 0 2.6-.6 4.9-2.3 5.9-4.1 1.1-2.1 1.7-7.5 1.5-9.9-.4-4.4-4.4-6.6-8.8-6.6s-8.4 2.2-8.8 6.6c-.2 2.3.3 7.8 1.5 9.9.9 1.8 3.3 3.5 5.9 4.1m4.9 39.7c-.9 9.2-1.2 18.3-1.9 27.4-.1 1.6-.3 3.5-.2 5.1 0 .6.2 1.1.6 1.2h10.5c.7-.1.8-3.4-.4-3.9l-1.5-1 4.4-27.2c.9-6-11.3-8.2-11.5-1.6m-7 0c-.2-6.6-12.4-4.4-11.4 1.7l4.4 27.2-1.5 1c-1.1.5-1.1 3.9-.4 3.9h10.5c.4-.1.5-.6.6-1.2.1-1.6-.1-3.5-.2-5.1-.7-9.2-1.1-18.3-2-27.5"
                                  clip-path="url(#b)"/>
                            <path fill="#FFF"
                                  d="M72.7 54.8H52.2V52h20.4v2.8zm0 4.6H52.2v-2.8h20.4v2.8zm0 4.7H52.2v-2.8h20.4v2.8zm0 4.7H52.2V66h20.4v2.8zm0 4.7H52.2v-2.8h20.4v2.8zm0 4.7H52.2v-2.8h20.4v2.8zm0 4.6H52.2V80h20.4v2.8z"/>
                        </svg>

                    </div>
                    <div class="quater-box _lb"
                         style="background-image: url('<?= get_template_directory_uri(); ?>/dist/images/ofice-top-four.jpg')">
                        <svg class="quater-box-info" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 208.6 208.6">
                            <path fill="#00B188" d="M0 0h208.6v208.6H0z"/>
                            <text transform="translate(10.69 177.912)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">U</tspan>
                                <tspan x="15.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">K</tspan>
                                <tspan x="29.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Ł</tspan>
                                <tspan x="41" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">A</tspan>
                                <tspan x="55.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">D</tspan>
                            </text>
                            <text fill="#FFF" transform="translate(86.48 177.912)" font-family="'Lato-Bold'"
                                  font-size="21">

                            </text>
                            <text transform="translate(10.69 200.907)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">GA</tspan>
                                <tspan x="29.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">BI</tspan>
                                <tspan x="49.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">N</tspan>
                                <tspan x="65.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">E</tspan>
                                <tspan x="77.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">T</tspan>
                                <tspan x="89.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">O</tspan>
                                <tspan x="105.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">W</tspan>
                                <tspan x="128.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Y</tspan>
                            </text>
                            <path fill="#FFF" d="M46.8 84.7h96.9v5.4H46.8z"/>
                            <defs>
                                <path id="a" d="M0 0h208.6v208.6H0z"/>
                            </defs>
                            <clipPath id="b">
                                <use xlink:href="#a" overflow="visible"/>
                            </clipPath>
                            <path fill="#FFF"
                                  d="M113.1 84.7H88.5l-1.2-16.3h26.9l-1.1 16.3zm6.8-14.8c-.8-5.6-4.6-10.3-10.3-10.3H91.9c-5.7 0-9.5 4.7-10.3 10.3l-2.3 15.8h43l-2.4-15.8zM99.4 57.8c1.3.2 1.6.2 2.8 0 2.6-.6 4.9-2.3 5.9-4.1 1.1-2.1 1.7-7.5 1.5-9.9-.4-4.4-4.4-6.6-8.8-6.6s-8.4 2.2-8.8 6.6c-.2 2.3.3 7.8 1.5 9.9.9 1.8 3.3 3.5 5.9 4.1m4.9 39.7c-.9 9.2-1.2 18.3-1.9 27.4-.1 1.6-.3 3.5-.2 5.1 0 .6.2 1.1.6 1.2h10.5c.7-.1.8-3.4-.4-3.9l-1.5-1 4.4-27.2c.9-6-11.3-8.2-11.5-1.6m-7 0c-.2-6.6-12.4-4.4-11.4 1.7l4.4 27.2-1.5 1c-1.1.5-1.1 3.9-.4 3.9h10.5c.4-.1.5-.6.6-1.2.1-1.6-.1-3.5-.2-5.1-.7-9.2-1.1-18.3-2-27.5"
                                  clip-path="url(#b)"/>
                            <path fill="#FFF"
                                  d="M72.7 54.8H52.2V52h20.4v2.8zm0 4.6H52.2v-2.8h20.4v2.8zm0 4.7H52.2v-2.8h20.4v2.8zm0 4.7H52.2V66h20.4v2.8zm0 4.7H52.2v-2.8h20.4v2.8zm0 4.7H52.2v-2.8h20.4v2.8zm0 4.6H52.2V80h20.4v2.8z"/>
                        </svg>

                    </div>
                </div>

            </div>
            <div class="swiper-slide">
                <div class="section-content floor-plans">
                    <div class="floor-container">
                        <img src="<?= get_template_directory_uri(); ?>/dist/images/offices-plan-one.jpg" alt=""
                             id="current-floor">
                        <div class="floor-info">
                            <p id="floor-info-txt" class="title">
                                Plan przykładowej aranżacji dla 1 najemcy <br> Budynek A
                            </p>
                            <p class="meta">
                                <span class="legend-square"></span> 1 najemca - 1628,90 m2
                            </p>
                        </div>
                    </div>
                    <div class="floors-nav">
                        <a class="floor-link" href="">
                            <img class="floor-img"
                                 src="<?= get_template_directory_uri(); ?>/dist/images/offices-plan-two.jpg" alt="">
                            <p class="floor-desc">
                                PLAN PRZYKŁADOWEJ ARANŻACJI dla 4 najemcy BUDYNEK A
                            </p>
                        </a>
                        <a class="floor-link" href="">
                            <img class="floor-img"
                                 src="<?= get_template_directory_uri(); ?>/dist/images/offices-plan-three.jpg" alt="">
                            <p class="floor-desc">
                                PLAN PRZYKŁADOWEJ ARANŻACJI dla 1 najemcy BUDYNEK B
                            </p>
                        </a>
                        <a class="floor-link" href="">
                            <img class="floor-img"
                                 src="<?= get_template_directory_uri(); ?>/dist/images/offices-plan-four.jpg" alt="">
                            <p class="floor-desc">
                                PLAN PRZYKŁADOWEJ ARANŻACJI dla 4 najemcy BUDYNEK B
                            </p>
                        </a>
                    </div>

                </div>


            </div>
            <div class="swiper-slide">
                <div class="section-content facilities-section">
                    <div class="legend-panel">
                        <img src="<?= get_template_directory_uri();?>/dist/images/facilities-legend.jpg" alt="">
                    </div>
                    <div class="map-container">

                    </div>

                </div>
            </div>
            <div class="swiper-slide">
                <div class="section-content panel-four-section">

                </div>
            </div>
            <!-- Add Pagination -->

        </div>
    </div>

<?php endwhile; ?>