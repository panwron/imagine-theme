/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
          var swiper = new Swiper('.swiper-container', {
              pagination: '.swiper-pagination',
              paginationClickable: true,
              direction: 'vertical',
              mousewheelControl: true,
              speed: 1000,
              preloadImages: false,
              lazyLoadingInPrevNext: true

          });

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired


          $('#menu-toggle').on('click', function (e) {
              e.preventDefault();
              $(this).toggleClass('open');

              var menu = $('#menu-main'),
                  lang = $('.lang-switch.xs'),
                  nav  = $('.nav-primary');



              if(!nav.hasClass('visible')){
                  nav.addClass('visible');
                  setTimeout(function () {
                      menu.addClass('visible');
                      lang.addClass('visible');
                  },300)

              }else{
                  menu.removeClass('visible');
                  lang.removeClass('visible');

                  setTimeout(function () {
                      nav.removeClass('visible');
                  },300)


              }

          })


      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    'biura':{
      init: function () {
          console.log("biura");

          function floorPlans() {
              var $floorContainer = $('#current-floor'),
                  $floorInfo = $('#floor-info-txt');
                  floors = $('.floor-link');

              floors.on('click', function (e) {
                  e.preventDefault();
                  var floorImg  = $(this).children('.floor-img').attr('src'),
                      floorDesc = $(this).children('.floor-desc').text();

                  $floorContainer.fadeOut(600, function () {
                      $floorContainer.attr('src', floorImg);
                  }).fadeIn(500);

                  $floorInfo.html(floorDesc);


                  console.log(floorImg);


              })

          }

          floorPlans();










      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
